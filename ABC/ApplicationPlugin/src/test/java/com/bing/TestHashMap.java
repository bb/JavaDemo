package com.bing;

import org.junit.Assert;
import org.junit.Test;

import java.util.HashMap;

/**
 * Created by Administrator on 2015/5/25.
 */
public class TestHashMap {

    @Test
    public void testHello() {
        HashMap<String, String> map = new HashMap();
        map.put("abc", "abcValue");
        map.put(null, "nullValue");
        map.put("cde", "cdeValue");
        map.put(null, "nullValue2");

        Assert.assertEquals("abcValue", map.get("abc"));

    }
}
