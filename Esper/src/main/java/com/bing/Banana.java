package com.bing;

/**
 * Created by Administrator on 2015/5/16.
 */
public class Banana {
    private int price;

    public Banana(int price) {
        this.price = price;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}
