package com.bing;

/**
 * Created by Administrator on 2015/5/16.
 */
public class ComputeArea {

    private int length;
    private int width;

    public ComputeArea(int width, int length) {
        this.width = width;
        this.length = length;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getArea() {
        System.out.println("getArea() 被调用");
        return length * width;
    }

    public int getArea(int width, int length) {
        System.out.printf("getArea(%d,%d) 被调用%n", width, length);

        return width * length;
    }

    public static int getSarea(int width, int length) {
        System.out.printf("getSArea(%d,%d) 被调用%n", width, length);
        return width * length;

    }

}
