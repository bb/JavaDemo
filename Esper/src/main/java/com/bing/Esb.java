package com.bing;

import com.espertech.esper.client.EventBean;

/**
 * Created by Administrator on 2015/5/16.
 */
public class Esb {
    private int id;
    private int price;

    public Esb(int price, int id) {
        this.price = price;
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public static String toString(EventBean eventBean) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("context.Id:").append(eventBean.get("id")).append("  context.Name:").append(eventBean.get("name"))
                .append(" context.Kye1:").append(eventBean.get("key1")).append(" context.Key2:").append(eventBean.get("key2"));
        return stringBuilder.toString();
    }
}
