package com.bing.eq;

import java.util.Random;

/**
 * 验证 hashcode 值不一样, 即使equals 返回true.也不=
 * Created by Administrator on 2015/8/30.
 */
public class HelloEquals {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        HelloEquals that = (HelloEquals) o;

        return !(name != null ? !name.equals(that.name) : that.name != null);

    }

    @Override
    public int hashCode() {
        return new Random().nextInt();
    }
}
