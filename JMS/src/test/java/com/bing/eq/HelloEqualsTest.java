package com.bing.eq;

import junit.framework.Assert;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Administrator on 2015/8/30.
 */
public class HelloEqualsTest {

    @Test
    public void testEquals() throws Exception {
        HelloEquals helloEquals1 = new HelloEquals();
        helloEquals1.setName("bing");
        HelloEquals helloEquals2 = new HelloEquals();
        helloEquals2.setName("bing");
        Assert.assertTrue(helloEquals1.equals(helloEquals2));
    }


    /**
     *  equals 即使返回true, hashcode值不一样,在hashSet中会先根据hashcode值查找.
     *  hashcode值不一样,就会认定对象不等.
     */
    @Test
    public void testHashEquals(){
        HelloEquals helloEquals1 = new HelloEquals();
        helloEquals1.setName("bing");
        HelloEquals helloEquals2 = new HelloEquals();
        helloEquals2.setName("bing");

        Set<HelloEquals> helloEqualsMap=new HashSet<>();
        helloEqualsMap.add(helloEquals1);
        helloEqualsMap.add(helloEquals2);
        System.out.println(helloEqualsMap.contains(helloEquals1));
        System.out.println(helloEqualsMap.contains(helloEquals2));
        System.out.println(helloEqualsMap.size());
    }
}