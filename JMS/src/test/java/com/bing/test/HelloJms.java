package com.bing.test;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.command.ActiveMQQueue;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.jms.*;

/**
 * Created by Administrator on 2015/5/10.
 */
public class HelloJms {
    ConnectionFactory connectionFactory = new ActiveMQConnectionFactory("vm://localhost");

    Connection connection;

    Queue queue;


    @Before
    public void before() throws JMSException {
        connection = connectionFactory.createConnection();
        connection.start();
        queue = new ActiveMQQueue("testQueue");
    }

    @After
    public void after() throws JMSException {
        connection.close();
    }

    @Test
    public void testHello() {
        System.out.println("世界你好!");
    }

    @Test
    public void testJms() throws JMSException {
        final Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        Message msg = session.createTextMessage("hello jms");
        MessageProducer messageProducer = session.createProducer(queue);

        messageProducer.send(msg);

        System.out.println("send Message complete");
        MessageConsumer messageConsumer = session.createConsumer(queue);
        Message rcMsg = messageConsumer.receive();
        System.out.println(((TextMessage) rcMsg).getText());

    }

    @Test
    public void testJMS2() throws JMSException {
        final Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        Message msg = session.createTextMessage("hello jms");

        MessageConsumer messageConsumer = session.createConsumer(queue);
        messageConsumer.setMessageListener(new MessageListener() {
            @Override
            public void onMessage(Message message) {
                try {
                    System.out.println("收到消息 ");
                    System.out.println(((TextMessage) message).getText());
                } catch (JMSException e) {
                    e.printStackTrace();
                }
            }
        });

        MessageProducer messageProducer = session.createProducer(queue);
        messageProducer.send(msg);
    }

    @Test
    public void testJMS3() throws JMSException {
        final Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

        MessageConsumer messageConsumer = session.createConsumer(queue);
        messageConsumer.setMessageListener(new MessageListener() {
            @Override
            public void onMessage(Message message) {
                try {
                    System.out.println("consumer1:收到消息 ");
                    System.out.println(((TextMessage) message).getText());
                } catch (JMSException e) {
                    e.printStackTrace();
                }
            }
        });


        MessageConsumer messageConsumer2 = session.createConsumer(queue);
        messageConsumer2.setMessageListener(new MessageListener() {
            @Override
            public void onMessage(Message message) {
                try {
                    System.out.println("consumer2:收到消息 ");
                    System.out.println(((TextMessage) message).getText());
                } catch (JMSException e) {
                    e.printStackTrace();
                }
            }
        });
        MessageProducer messageProducer = session.createProducer(queue);
        for (int i = 0; i <= 20; i++) {
            messageProducer.send(session.createTextMessage(String.format("hello Jms%d", i)));
        }
    }
}
