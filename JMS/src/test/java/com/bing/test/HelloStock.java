package com.bing.test;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.Test;

import javax.script.ScriptException;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Administrator on 2015/5/27.
 */
public class HelloStock {


    @Test
    public void test() throws IOException, ScriptException {
        Document doc = Jsoup.connect("http://hq.sinajs.cn/list=sh601003,sh601001,sh601002,sh601005,sh601008,sh601009").ignoreContentType(true).get();
        String text = doc.text();
        System.out.println(text);
        Matcher matcher = Pattern.compile("hq_str_(?<daihao>.*?)=\"(?<detail>[^\"]*?)\";").matcher(text);
        while (matcher.find()) {
            System.out.println("代号: " + matcher.group("daihao"));
            System.out.println("详情:  " + matcher.group("detail"));
        }

    }
}
