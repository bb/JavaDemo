package com.bing.test;

import org.junit.Test;

/**
 * Created by Administrator on 2015/5/17.
 */
public class HelloGc {
    private static final int _1MB = 1024 * 1024;

    /**
     * Vm 参数: -verbose:gc -Xms20M -Xmx20M -Xmn10M -XX:+PrintGCDetails -XX:SurvivorRatio=8
     * 新生代10M,老年代10M.
     * <p/>
     * <p/>
     * [GC (Allocation Failure) [DefNew: 7346K->1024K(9216K), 0.0052843 secs] 7346K->3364K(19456K), 0.0985931 secs] [Times: user=0.00 sys=0.00, real=0.10 secs]
     * [GC (Allocation Failure) [DefNew: 5278K->0K(9216K), 0.0076055 secs] 7619K->7460K(19456K), 0.0076406 secs] [Times: user=0.00 sys=0.00, real=0.01 secs]
     * Heap
     * def new generation   total 9216K, used 4391K [0x04a00000, 0x05400000, 0x05400000)
     * eden space 8192K,  53% used [0x04a00000, 0x04e49d00, 0x05200000)
     * from space 1024K,   0% used [0x05200000, 0x05200000, 0x05300000)
     * to   space 1024K,   0% used [0x05300000, 0x05300000, 0x05400000)
     * tenured generation   total 10240K, used 7460K [0x05400000, 0x05e00000, 0x05e00000)
     * the space 10240K,  72% used [0x05400000, 0x05b493b8, 0x05b49400, 0x05e00000)
     * Metaspace       used 2938K, capacity 2998K, committed 3008K, reserved 4480K
     */
    @Test
    public void testAllocation() {
        byte[] a1, a2, a3, a4;
        a1 = new byte[2 * _1MB];
        a2 = new byte[2 * _1MB];
        a3 = new byte[2 * _1MB];
        a4 = new byte[4 * _1MB];  //出现一次MinorGc
    }


    /**
     * VM 参数: -verbose:gc -Xms20M -Xmx20M -Xmn10M -XX:PrintGCDetails --XX:SurvivorRatio=8 -XX:PretenureSizeThreshold=3145728
     *
     *  tenured generation 用了40%
     * Heap     def new generation   total 9216K, used 5462K [0x04a00000, 0x05400000, 0x05400000)
     eden space 8192K,  66% used [0x04a00000, 0x04f55b90, 0x05200000)
     from space 1024K,   0% used [0x05200000, 0x05200000, 0x05300000)
     to   space 1024K,   0% used [0x05300000, 0x05300000, 0x05400000)
     tenured generation   total 10240K, used 4096K [0x05400000, 0x05e00000, 0x05e00000)
     the space 10240K,  40% used [0x05400000, 0x05800010, 0x05800200, 0x05e00000)
     Metaspace       used 2937K, capacity 2998K, committed 3008K, reserved 4480K
     */
    @Test
    public void testPretenureSizeThreshold() {
        byte[] al;
        al = new byte[4 * _1MB];
    }

}
