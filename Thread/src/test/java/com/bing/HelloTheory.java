package com.bing;

import org.junit.experimental.theories.DataPoint;
import org.junit.experimental.theories.Theories;
import org.junit.experimental.theories.Theory;
import org.junit.experimental.theories.suppliers.TestedOn;
import org.junit.runner.RunWith;

/**
 * Created by Administrator on 2015/5/23.
 */
@RunWith(Theories.class)
public class HelloTheory {

    @DataPoint
    public static String m1 = "abc1";

    @DataPoint
    public static String abc2 = "abc2";
    @DataPoint
    public static String m3 = "abc3";


    @Theory
    public void testTheory(String s) {
        System.out.println(s);
    }


    @Theory
    public void testTestOn(@TestedOn(ints = {0, 1, 2}) int i) {
        System.out.println(i);
    }

}
