package com.bing.impl;

import com.bing.entity.Person;

/**
 * Created by Administrator on 2015/8/16.
 */
public class InShiYi implements ShiYi {

    private Person person;

    public InShiYi(Person person) {
        this.person = person;
    }

    @Override
    public void cuanYi() {
        System.out.println("person: " + person.toString());
    }
}
