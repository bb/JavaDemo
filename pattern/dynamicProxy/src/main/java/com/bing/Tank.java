package com.bing;

import java.util.Random;

/**
 * Created by Administrator on 2015/5/19.
 */
public class Tank implements MoveAble {
    @Override
    public void move() {
        System.out.println("Tank Moving.....");
        try {
            Thread.sleep(new Random().nextInt(10000));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
