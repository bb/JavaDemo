package com.bing;

/**
 * Created by Administrator on 2015/5/19.
 * 聚合
 */
public class Task3 implements MoveAble {

    Tank t;

    public Task3(Tank t) {
        this.t = t;
    }

    @Override
    public void move() {
        long start = System.currentTimeMillis();
        t.move();
        long end = System.currentTimeMillis();
        System.out.println("time" + (end - start));
    }
}
