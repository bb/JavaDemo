package com.bing.bean;

import java.util.Date;
import java.util.List;

/**
 * Created by Administrator on 2015/5/30.
 */
public class AllStock {

    private String code;
    private String name;
    private List<String> banKuai;
    private Date date;


    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("AllStock{");
        sb.append("code='").append(code).append('\'');
        sb.append(", name='").append(name).append('\'');
        sb.append(", banKuai=").append(banKuai);
        sb.append(", date=").append(date);
        sb.append('}');
        return sb.toString();
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getBanKuai() {
        return banKuai;
    }

    public void setBanKuai(List<String> banKuai) {
        this.banKuai = banKuai;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
