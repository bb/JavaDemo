package com.bing.bean;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Administrator on 2015/5/27.
 * 股票1.1Sina股票数据接口
 * 以大秦铁路（股票代码：601006）为例，如果要获取它的最新行情，只需访问新浪的股票数据接口：
 * http://hq.sinajs.cn/list=sh601006
 * 这个url会返回一串文本，例如：
 * var hq_str_sh601006="大秦铁路, 27.55, 27.25, 26.91, 27.55, 26.20, 26.91, 26.92,
 * 22114263, 589824680, 4695, 26.91, 57590, 26.90, 14700, 26.89, 14300,
 * 26.88, 15100, 26.87, 3100, 26.92, 8900, 26.93, 14230, 26.94, 25150, 26.95, 15220, 26.96, 2008-01-11, 15:05:32";
 * 这个字符串由许多数据拼接在一起，不同含义的数据用逗号隔开了，按照程序员的思路，顺序号从0开始。
 * 0：”大秦铁路”，股票名字；
 * 1：”27.55″，今日开盘价；
 * 2：”27.25″，昨日收盘价；
 * 3：”26.91″，当前价格；
 * 4：”27.55″，今日最高价；
 * 5：”26.20″，今日最低价；
 * 6：”26.91″，竞买价，即“买一”报价；
 * 7：”26.92″，竞卖价，即“卖一”报价；
 * 8：”22114263″，成交的股票数，由于股票交易以一百股为基本单位，所以在使用时，通常把该值除以一百；
 * 9：”589824680″，成交金额，单位为“元”，为了一目了然，通常以“万元”为成交金额的单位，所以通常把该值除以一万；
 * 10：”4695″，“买一”申请4695股，即47手；
 * 11：”26.91″，“买一”报价；
 * 12：”57590″，“买二”
 * 13：”26.90″，“买二”
 * 14：”14700″，“买三”
 * 15：”26.89″，“买三”
 * 16：”14300″，“买四”
 * 17：”26.88″，“买四”
 * 18：”15100″，“买五”
 * 19：+?.,mnbvzu81  26.87″，“买五”
 * 20：”3100″，“卖一”申报3100股，即31手；
 * 21：”26.92″，“卖一”报价
 * (22, 23), (24, 25), (26,27), (28, 29)分别为“卖二”至“卖四的情况”
 * 30：”2008-01-11″，日期；
 * 31：”15:05:32″，时间；
 */
public class Stock {
    private final static SimpleDateFormat sdf= new SimpleDateFormat("yyyy-MM-ddHH:mm:ss");
    private String code;
    private String name;
    private double openningPrice;//今日开盘价
    private double closingPrice; //昨日收盘价
    private double currentPrice;//当前价格;
    private double hPrice;//今日最高价;
    private double lPrice;//今日最低价;
    private double competitivePrice;//竞买价,即"买一"报价;
    private double auctionPrice;//竞买价,即"买一"报价;
    private long totalNumber;//成交股数,由于股票交易以一百股为基本单位，所以在使用时，通常把该值除以一百；
    private Double turnover;//成交金额，单位为“元”，为了一目了然，通常以“万元”为成交金额的单位，所以通常把该值除以一万；
    private long buy1;//“买一”申请4695股，即47手；
    private double buy1Price; //“买一”报价
    private long buy2;//“买二”申请4695股，即47手；
    private double buy2Price; //“买二”报价；
    private long buy3;//“买三”申请4695股，即47手；
    private double buy3Price; //“买三”报价；
    private long buy4;//“买四”申请4695股，即47手；
    private double buy4Price; //“买四”报价；
    private long buy5;//“买五”申请4695股，即47手；
    private double buy5Price; //“买五”报价；

    private long sell1;//“卖一”申请4695股，即47手；
    private double sell1Price; //“卖一一”报价；
    private long sell2;//“卖一二”申请4695股，即47手；
    private double sell2Price; //“卖一二”报价；
    private long sell3;//“卖一三”申请4695股，即47手；
    private double sell3Price; //“卖一三”报价；
    private long sell4;//“卖一四”申请4695股，即47手；
    private double sell4Price; //“卖一四”报价；
    private long sell5;//“卖一五”申请4695股，即47手；
    private double sell5Price; //“卖一五”报价；
    private Date date;// 日期时间  2008-01-11 15:05:32

    public Stock(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public Stock(String code) {
        this.code = code;
    }

    /**
     * hq_str_sh601003="柳钢股份,5.93,6.02,5.93,6.08,5.73,5.93,5.94,65557357,386864430,58300,5.93,102079,5.92,580900,5.91,581100,5.90,125600,5.89,135900,5.94,217826,5.95,105620,5.96,50690,5.97,122800,5.98,2015-05-27,15:05:06,00"
     *
     * @param code
     * @param detail
     * @return
     */
    public static Stock fromString(String code, String detail) {
        Stock stock = new Stock(code);
        String[] details = detail.split(",");
        stock.setName(details[0]);
        stock.setOpenningPrice(Double.valueOf(details[1]));
        stock.setClosingPrice(Double.valueOf(details[2]));
        stock.setCurrentPrice(Double.valueOf(details[3]));
        stock.sethPrice(Double.valueOf(details[4]));
        stock.setlPrice(Double.valueOf(details[5]));
        stock.setCompetitivePrice(Double.valueOf(details[6]));
        stock.setAuctionPrice(Double.valueOf(details[7]));
        stock.setTotalNumber(Long.valueOf(details[8]));
        stock.setTurnover(Double.valueOf(details[9]));
        stock.setBuy1(Long.valueOf(details[10]));
        stock.setBuy1Price(Double.valueOf(details[11]));
        stock.setBuy2(Long.valueOf(details[12]));
        stock.setBuy2Price(Double.valueOf(details[13]));
        stock.setBuy3(Long.valueOf(details[14]));
        stock.setBuy3Price(Double.valueOf(details[15]));
        stock.setBuy4(Long.valueOf(details[16]));
        stock.setBuy4Price(Double.valueOf(details[17]));
        stock.setBuy5(Long.valueOf(details[18]));
        stock.setBuy5Price(Double.valueOf(details[19]));

        stock.setSell1(Long.valueOf(details[20]));
        stock.setSell1Price(Double.valueOf(details[21]));
        stock.setSell2(Long.valueOf(details[22]));
        stock.setSell2Price(Double.valueOf(details[23]));
        stock.setSell3(Long.valueOf(details[24]));
        stock.setSell3Price(Double.valueOf(details[25]));
        stock.setSell4(Long.valueOf(details[26]));
        stock.setSell4Price(Double.valueOf(details[27]));
        stock.setSell5(Long.valueOf(details[28]));
        stock.setSell5Price(Double.valueOf(details[29]));

        try {
            stock.setDate(sdf.parse(details[30]+details[31]));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return stock;
    }

    public Double getTurnover() {
        return turnover;
    }

    public void setTurnover(Double turnover) {
        this.turnover = turnover;
    }

    public long getBuy2() {
        return buy2;
    }

    public void setBuy2(long buy2) {
        this.buy2 = buy2;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Stock{");
        sb.append("code='").append(code).append('\'');
        sb.append(", name='").append(name).append('\'');
        sb.append(", openningPrice=").append(openningPrice);
        sb.append(", closingPrice=").append(closingPrice);
        sb.append(", currentPrice=").append(currentPrice);
        sb.append(", hPrice=").append(hPrice);
        sb.append(", lPrice=").append(lPrice);
        sb.append(", competitivePrice=").append(competitivePrice);
        sb.append(", auctionPrice=").append(auctionPrice);
        sb.append(", totalNumber=").append(totalNumber);
        sb.append(", turnover=").append(turnover);
        sb.append(", buy1=").append(buy1);
        sb.append(", buy1Price=").append(buy1Price);
        sb.append(", buy2=").append(buy2);
        sb.append(", buy2Price=").append(buy2Price);
        sb.append(", buy3=").append(buy3);
        sb.append(", buy3Price=").append(buy3Price);
        sb.append(", buy4=").append(buy4);
        sb.append(", buy4Price=").append(buy4Price);
        sb.append(", buy5=").append(buy5);
        sb.append(", buy5Price=").append(buy5Price);
        sb.append(", sell1=").append(sell1);
        sb.append(", sell1Price=").append(sell1Price);
        sb.append(", sell2=").append(sell2);
        sb.append(", sell2Price=").append(sell2Price);
        sb.append(", sell3=").append(sell3);
        sb.append(", sell3Price=").append(sell3Price);
        sb.append(", sell4=").append(sell4);
        sb.append(", sell4Price=").append(sell4Price);
        sb.append(", sell5=").append(sell5);
        sb.append(", sell5Price=").append(sell5Price);
        sb.append(", date=").append(date);
        sb.append('}');
        return sb.toString();
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getOpenningPrice() {
        return openningPrice;
    }

    public void setOpenningPrice(double openningPrice) {
        this.openningPrice = openningPrice;
    }

    public double getClosingPrice() {
        return closingPrice;
    }

    public void setClosingPrice(double closingPrice) {
        this.closingPrice = closingPrice;
    }

    public double getCurrentPrice() {
        return currentPrice;
    }

    public void setCurrentPrice(double currentPrice) {
        this.currentPrice = currentPrice;
    }

    public double gethPrice() {
        return hPrice;
    }

    public void sethPrice(double hPrice) {
        this.hPrice = hPrice;
    }

    public double getlPrice() {
        return lPrice;
    }

    public void setlPrice(double lPrice) {
        this.lPrice = lPrice;
    }

    public double getCompetitivePrice() {
        return competitivePrice;
    }

    public void setCompetitivePrice(double competitivePrice) {
        this.competitivePrice = competitivePrice;
    }

    public double getAuctionPrice() {
        return auctionPrice;
    }

    public void setAuctionPrice(double auctionPrice) {
        this.auctionPrice = auctionPrice;
    }

    public long getTotalNumber() {
        return totalNumber;
    }

    public void setTotalNumber(long totalNumber) {
        this.totalNumber = totalNumber;
    }

     public long getBuy1() {
        return buy1;
    }

    public void setBuy1(long buy1) {
        this.buy1 = buy1;
    }

    public double getBuy1Price() {
        return buy1Price;
    }

    public void setBuy1Price(double buy1Price) {
        this.buy1Price = buy1Price;
    }

    public double getBuy2Price() {
        return buy2Price;
    }

    public void setBuy2Price(double buy2Price) {
        this.buy2Price = buy2Price;
    }

    public long getBuy3() {
        return buy3;
    }

    public void setBuy3(long buy3) {
        this.buy3 = buy3;
    }

    public double getBuy3Price() {
        return buy3Price;
    }

    public void setBuy3Price(double buy3Price) {
        this.buy3Price = buy3Price;
    }

    public long getBuy4() {
        return buy4;
    }

    public void setBuy4(long buy4) {
        this.buy4 = buy4;
    }

    public double getBuy4Price() {
        return buy4Price;
    }

    public void setBuy4Price(double buy4Price) {
        this.buy4Price = buy4Price;
    }

    public long getBuy5() {
        return buy5;
    }

    public void setBuy5(long buy5) {
        this.buy5 = buy5;
    }

    public double getBuy5Price() {
        return buy5Price;
    }

    public void setBuy5Price(double buy5Price) {
        this.buy5Price = buy5Price;
    }

    public long getSell1() {
        return sell1;
    }

    public void setSell1(long sell1) {
        this.sell1 = sell1;
    }

    public double getSell1Price() {
        return sell1Price;
    }

    public void setSell1Price(double sell1Price) {
        this.sell1Price = sell1Price;
    }

    public long getSell2() {
        return sell2;
    }

    public void setSell2(long sell2) {
        this.sell2 = sell2;
    }

    public double getSell2Price() {
        return sell2Price;
    }

    public void setSell2Price(double sell2Price) {
        this.sell2Price = sell2Price;
    }

    public long getSell3() {
        return sell3;
    }

    public void setSell3(long sell3) {
        this.sell3 = sell3;
    }

    public double getSell3Price() {
        return sell3Price;
    }

    public void setSell3Price(double sell3Price) {
        this.sell3Price = sell3Price;
    }

    public long getSell4() {
        return sell4;
    }

    public void setSell4(long sell4) {
        this.sell4 = sell4;
    }

    public double getSell4Price() {
        return sell4Price;
    }

    public void setSell4Price(double sell4Price) {
        this.sell4Price = sell4Price;
    }

    public long getSell5() {
        return sell5;
    }

    public void setSell5(long sell5) {
        this.sell5 = sell5;
    }

    public double getSell5Price() {
        return sell5Price;
    }

    public void setSell5Price(double sell5Price) {
        this.sell5Price = sell5Price;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
