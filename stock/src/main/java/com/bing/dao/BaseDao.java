package com.bing.dao;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Administrator on 2015/7/15.
 */
public interface BaseDao<T, K extends Serializable> {

    void save(T entity);

    void update(T entity);

    void delete(K id);

    T findById(K id);

    List<T> findByHQL(String hql, Object... params);

    T findByHQLOne(String hql, Object... params);
}
