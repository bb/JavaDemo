package com.bing.dao;

import com.bing.entity.Stock;

/**
 * Created by Administrator on 2015/7/14.
 */

public interface StockDao extends BaseDao<Stock, Long> {
    Stock getByName(String name);
}
