package com.bing.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;

/**
 * Created by Administrator on 2015/7/12.
 */
@Entity
@Table
public class BanKuai {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(nullable = false, length = 1024)
    private String name;

    @Column(length = 10000,name = "descs")
    private String desc;

    @ManyToOne()
    @JoinColumn(name = "ref_parent")
    private BanKuai parent;

    @OneToMany(mappedBy = "parent")
    private List<BanKuai> banKuaiList;

    @ManyToOne
    @JoinColumn(name = "ref_stock")
    private Stock stock;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public BanKuai getParent() {
        return parent;
    }

    public void setParent(BanKuai parent) {
        this.parent = parent;
    }

    public List<BanKuai> getBanKuaiList() {
        return banKuaiList;
    }

    public void setBanKuaiList(List<BanKuai> banKuaiList) {
        this.banKuaiList = banKuaiList;
    }

    public Stock getStock() {
        return stock;
    }

    public void setStock(Stock stock) {
        this.stock = stock;
    }
}
