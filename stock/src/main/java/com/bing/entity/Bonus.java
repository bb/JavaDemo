package com.bing.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;

/**
 * Created by Administrator on 2015/7/12.
 */

@Entity
@Table
public class Bonus {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne()
    @JoinColumn(name = "ref_stock")
    private Stock stock;

    @Column(name = "dates")
    @Temporal(TemporalType.DATE)
    private Date date;

    @Column(name = "descs")
    private String desc;//分红方案说明

    @Column(name = "djDate")
    @Temporal(TemporalType.DATE)
    private Date djDate;//A股股权登记日

    @Column(name = "cqDate")
    @Temporal(TemporalType.DATE)
    private Date cqDate;//A股除权除息日

    @Column(name = "pxDate")
    @Temporal(TemporalType.DATE)
    private Date pxDate;//A股派息日

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Stock getStock() {
        return stock;
    }

    public void setStock(Stock stock) {
        this.stock = stock;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public Date getDjDate() {
        return djDate;
    }

    public void setDjDate(Date djDate) {
        this.djDate = djDate;
    }

    public Date getCqDate() {
        return cqDate;
    }

    public void setCqDate(Date cqDate) {
        this.cqDate = cqDate;
    }

    public Date getPxDate() {
        return pxDate;
    }

    public void setPxDate(Date pxDate) {
        this.pxDate = pxDate;
    }
}
