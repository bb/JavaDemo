package com.bing.entity;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Administrator on 2015/7/12.
 */

@Entity
@Table
public class Stock {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(nullable = false, length = 1024)
    private String name;

    @Column(nullable = false, length = 1024, unique = true)
    private String code;

    @ElementCollection
    @CollectionTable(name = "pinYin", joinColumns = @JoinColumn(name = "stock_id"))
    @Column(name = "pinYin")
    private Set<String> pinYin = new HashSet<String>();

    @OneToMany(mappedBy = "stock")
    private List<BanKuai> banKuaiList = new ArrayList<BanKuai>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Set getPinYin() {
        return pinYin;
    }

    public void setPinYin(Set pinYin) {
        this.pinYin = pinYin;
    }

    public List<BanKuai> getBanKuaiList() {
        return banKuaiList;
    }

    public void setBanKuaiList(List<BanKuai> banKuaiList) {
        this.banKuaiList = banKuaiList;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Stock{");
        sb.append("id=").append(id);
        sb.append(", name='").append(name).append('\'');
        sb.append(", code='").append(code).append('\'');
        sb.append(", pinYin=").append(pinYin);
        sb.append(", banKuaiList=").append(banKuaiList);
        sb.append('}');
        return sb.toString();
    }
}
