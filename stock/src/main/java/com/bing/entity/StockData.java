package com.bing.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;

/**
 * Created by Administrator on 2015/7/12.
 */
@Entity
@Table
public class StockData {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "dates")
    @Temporal(TemporalType.DATE)
    private Date date;

    private double openningPrice;//今日开盘价

    private double closingPrice; //昨日收盘价

    private double currentPrice;//当前价格;

    private double hPrice;//今日最高价;

    private double lPrice;//今日最低价;

    private double competitivePrice;//竞买价,即"买一"报价;

    private double auctionPrice;//竞买价,即"买一"报价;

    private long totalNumber;//成交股数,由于股票交易以一百股为基本单位，所以在使用时，通常把该值除以一百；

    private double turnover;//成交金额，单位为“元”，为了一目了然，通常以“万元”为成交金额的单位，所以通常把该值除以一万；

    private long buy1;//“买一”申请4695股，即47手；

    private double buy1Price; //“买一”报价

    private long buy2;//“买二”申请4695股，即47手；

    private double buy2Price; //“买二”报价；

    private long buy3;//“买三”申请4695股，即47手；

    private double buy3Price; //“买三”报价；

    private long buy4;//“买四”申请4695股，即47手；

    private double buy4Price; //“买四”报价；

    private long buy5;//“买五”申请4695股，即47手；

    private double buy5Price; //“买五”报价；

    private long sell1;//“卖一”申请4695股，即47手；

    private double sell1Price; //“卖一一”报价；

    private long sell2;//“卖一二”申请4695股，即47手；

    private double sell2Price; //“卖一二”报价；

    private long sell3;//“卖一三”申请4695股，即47手；

    private double sell3Price; //“卖一三”报价；

    private long sell4;//“卖一四”申请4695股，即47手；

    private double sell4Price; //“卖一四”报价；

    private long sell5;//“卖一五”申请4695股，即47手；

    private double sell5Price; //“卖一五”报价；

    @ManyToOne()
    @JoinColumn(name = "ref_stock")
    private Stock stock;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public double getOpenningPrice() {
        return openningPrice;
    }

    public void setOpenningPrice(double openningPrice) {
        this.openningPrice = openningPrice;
    }

    public double getClosingPrice() {
        return closingPrice;
    }

    public void setClosingPrice(double closingPrice) {
        this.closingPrice = closingPrice;
    }

    public double getCurrentPrice() {
        return currentPrice;
    }

    public void setCurrentPrice(double currentPrice) {
        this.currentPrice = currentPrice;
    }

    public double gethPrice() {
        return hPrice;
    }

    public void sethPrice(double hPrice) {
        this.hPrice = hPrice;
    }

    public double getlPrice() {
        return lPrice;
    }

    public void setlPrice(double lPrice) {
        this.lPrice = lPrice;
    }

    public double getCompetitivePrice() {
        return competitivePrice;
    }

    public void setCompetitivePrice(double competitivePrice) {
        this.competitivePrice = competitivePrice;
    }

    public double getAuctionPrice() {
        return auctionPrice;
    }

    public void setAuctionPrice(double auctionPrice) {
        this.auctionPrice = auctionPrice;
    }

    public long getTotalNumber() {
        return totalNumber;
    }

    public void setTotalNumber(long totalNumber) {
        this.totalNumber = totalNumber;
    }

    public double getTurnover() {
        return turnover;
    }

    public void setTurnover(double turnover) {
        this.turnover = turnover;
    }

    public long getBuy1() {
        return buy1;
    }

    public void setBuy1(long buy1) {
        this.buy1 = buy1;
    }

    public double getBuy1Price() {
        return buy1Price;
    }

    public void setBuy1Price(double buy1Price) {
        this.buy1Price = buy1Price;
    }

    public long getBuy2() {
        return buy2;
    }

    public void setBuy2(long buy2) {
        this.buy2 = buy2;
    }

    public double getBuy2Price() {
        return buy2Price;
    }

    public void setBuy2Price(double buy2Price) {
        this.buy2Price = buy2Price;
    }

    public long getBuy3() {
        return buy3;
    }

    public void setBuy3(long buy3) {
        this.buy3 = buy3;
    }

    public double getBuy3Price() {
        return buy3Price;
    }

    public void setBuy3Price(double buy3Price) {
        this.buy3Price = buy3Price;
    }

    public long getBuy4() {
        return buy4;
    }

    public void setBuy4(long buy4) {
        this.buy4 = buy4;
    }

    public double getBuy4Price() {
        return buy4Price;
    }

    public void setBuy4Price(double buy4Price) {
        this.buy4Price = buy4Price;
    }

    public long getBuy5() {
        return buy5;
    }

    public void setBuy5(long buy5) {
        this.buy5 = buy5;
    }

    public double getBuy5Price() {
        return buy5Price;
    }

    public void setBuy5Price(double buy5Price) {
        this.buy5Price = buy5Price;
    }

    public long getSell1() {
        return sell1;
    }

    public void setSell1(long sell1) {
        this.sell1 = sell1;
    }

    public double getSell1Price() {
        return sell1Price;
    }

    public void setSell1Price(double sell1Price) {
        this.sell1Price = sell1Price;
    }

    public long getSell2() {
        return sell2;
    }

    public void setSell2(long sell2) {
        this.sell2 = sell2;
    }

    public double getSell2Price() {
        return sell2Price;
    }

    public void setSell2Price(double sell2Price) {
        this.sell2Price = sell2Price;
    }

    public long getSell3() {
        return sell3;
    }

    public void setSell3(long sell3) {
        this.sell3 = sell3;
    }

    public double getSell3Price() {
        return sell3Price;
    }

    public void setSell3Price(double sell3Price) {
        this.sell3Price = sell3Price;
    }

    public long getSell4() {
        return sell4;
    }

    public void setSell4(long sell4) {
        this.sell4 = sell4;
    }

    public double getSell4Price() {
        return sell4Price;
    }

    public void setSell4Price(double sell4Price) {
        this.sell4Price = sell4Price;
    }

    public long getSell5() {
        return sell5;
    }

    public void setSell5(long sell5) {
        this.sell5 = sell5;
    }

    public double getSell5Price() {
        return sell5Price;
    }

    public void setSell5Price(double sell5Price) {
        this.sell5Price = sell5Price;
    }

    public Stock getStock() {
        return stock;
    }

    public void setStock(Stock stock) {
        this.stock = stock;
    }
}
