package com.bing.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;

/**
 * Created by Administrator on 2015/7/12.
 */

@Entity
@Table
public class StockDay {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "ref_stock")
    private Stock stock;

    @Temporal(TemporalType.DATE)
    @Column(name = "dates")
    private Date date;

    @Column(scale = 2)
    private double zhangDieFu; //涨跌幅

    @Column(scale = 2)
    private double zuiXinJia;//最新价

    private double chengJiaLiang; //成交量

    @Column(scale = 2)
    private double xianLiang; //现量

    @Column(scale = 2)
    private double huanShouLv; //换手率

    @Column(scale = 2)
    private double zuoShou;//昨收

    @Column(scale = 2)
    private double kaiPan;//开盘

    @Column(scale = 2)
    private double zuiGao;//最高

    @Column(scale = 2)
    private double zuiDi;// 最低

    @Column(scale = 2)
    private double zhangSu;//涨速

    @Column(scale = 2)
    private double liangBi;//量比

    private double chengJiaoEr;//成交额

    @Column(scale = 2)
    private double zhangDieEr;//涨跌额

    @Column(scale = 2)
    private double jiaoBuy;//叫买

    @Column(scale = 2)
    private double jiaoSell;//叫卖

    private double maiLiang;//买量

    @Column(scale = 2)
    private double weiBi;//委比

    private double biShu;//笔数

    private double ziJinLiuLu;//资金流入

    private double ziJinLiuChu;//资金流出

    private double jinLiuLu;//净流入

    private double qunBiShou;//均笔手

    private double qunBiEr;//均笔额

    @Column(scale = 2)
    private double jinLiuLuZhangBi;//净注入占比

    private double daDanLiuLu;//大单流入

    private double jinDaDan;//净大单

    private double jinDaDanZhanBi;//净大单占比

    private double daDanChengJiaEr;//大单成交额

    private double daDanZhanBi;//大单占笔

    private double zhongDanLiuLu;//中单流入

    private double zhongDanLiuChu;//中单流出

    @Column(scale = 2)
    private double jinZhongDanZhangBi;//净中单占笔

    private double jinZhongDan;//净中单

    @Column(scale = 2)
    private double zhongDanChenJiaoEr; //中单成交额

    private double xiaoDanLiuLu;//小单流入

    private double xiaoDanLiuChu;//小单流出

    @Column(scale = 2)
    private double jinXiaoDanZhangBi;//净小单占笔

    private double jinXiaoDan;//净小单

    @Column(scale = 2)
    private double xiaoDanChenJiaoEr; //小单成交额


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Stock getStock() {
        return stock;
    }

    public void setStock(Stock stock) {
        this.stock = stock;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public double getZhangDieFu() {
        return zhangDieFu;
    }

    public void setZhangDieFu(double zhangDieFu) {
        this.zhangDieFu = zhangDieFu;
    }

    public double getZuiXinJia() {
        return zuiXinJia;
    }

    public void setZuiXinJia(double zuiXinJia) {
        this.zuiXinJia = zuiXinJia;
    }

    public double getChengJiaLiang() {
        return chengJiaLiang;
    }

    public void setChengJiaLiang(double chengJiaLiang) {
        this.chengJiaLiang = chengJiaLiang;
    }

    public double getXianLiang() {
        return xianLiang;
    }

    public void setXianLiang(double xianLiang) {
        this.xianLiang = xianLiang;
    }

    public double getHuanShouLv() {
        return huanShouLv;
    }

    public void setHuanShouLv(double huanShouLv) {
        this.huanShouLv = huanShouLv;
    }

    public double getZuoShou() {
        return zuoShou;
    }

    public void setZuoShou(double zuoShou) {
        this.zuoShou = zuoShou;
    }

    public double getKaiPan() {
        return kaiPan;
    }

    public void setKaiPan(double kaiPan) {
        this.kaiPan = kaiPan;
    }

    public double getZuiGao() {
        return zuiGao;
    }

    public void setZuiGao(double zuiGao) {
        this.zuiGao = zuiGao;
    }

    public double getZuiDi() {
        return zuiDi;
    }

    public void setZuiDi(double zuiDi) {
        this.zuiDi = zuiDi;
    }

    public double getZhangSu() {
        return zhangSu;
    }

    public void setZhangSu(double zhangSu) {
        this.zhangSu = zhangSu;
    }

    public double getLiangBi() {
        return liangBi;
    }

    public void setLiangBi(double liangBi) {
        this.liangBi = liangBi;
    }

    public double getChengJiaoEr() {
        return chengJiaoEr;
    }

    public void setChengJiaoEr(double chengJiaoEr) {
        this.chengJiaoEr = chengJiaoEr;
    }

    public double getZhangDieEr() {
        return zhangDieEr;
    }

    public void setZhangDieEr(double zhangDieEr) {
        this.zhangDieEr = zhangDieEr;
    }

    public double getJiaoBuy() {
        return jiaoBuy;
    }

    public void setJiaoBuy(double jiaoBuy) {
        this.jiaoBuy = jiaoBuy;
    }

    public double getJiaoSell() {
        return jiaoSell;
    }

    public void setJiaoSell(double jiaoSell) {
        this.jiaoSell = jiaoSell;
    }

    public double getMaiLiang() {
        return maiLiang;
    }

    public void setMaiLiang(double maiLiang) {
        this.maiLiang = maiLiang;
    }

    public double getWeiBi() {
        return weiBi;
    }

    public void setWeiBi(double weiBi) {
        this.weiBi = weiBi;
    }

    public double getBiShu() {
        return biShu;
    }

    public void setBiShu(double biShu) {
        this.biShu = biShu;
    }

    public double getZiJinLiuLu() {
        return ziJinLiuLu;
    }

    public void setZiJinLiuLu(double ziJinLiuLu) {
        this.ziJinLiuLu = ziJinLiuLu;
    }

    public double getZiJinLiuChu() {
        return ziJinLiuChu;
    }

    public void setZiJinLiuChu(double ziJinLiuChu) {
        this.ziJinLiuChu = ziJinLiuChu;
    }

    public double getJinLiuLu() {
        return jinLiuLu;
    }

    public void setJinLiuLu(double jinLiuLu) {
        this.jinLiuLu = jinLiuLu;
    }

    public double getQunBiShou() {
        return qunBiShou;
    }

    public void setQunBiShou(double qunBiShou) {
        this.qunBiShou = qunBiShou;
    }

    public double getQunBiEr() {
        return qunBiEr;
    }

    public void setQunBiEr(double qunBiEr) {
        this.qunBiEr = qunBiEr;
    }

    public double getJinLiuLuZhangBi() {
        return jinLiuLuZhangBi;
    }

    public void setJinLiuLuZhangBi(double jinLiuLuZhangBi) {
        this.jinLiuLuZhangBi = jinLiuLuZhangBi;
    }

    public double getDaDanLiuLu() {
        return daDanLiuLu;
    }

    public void setDaDanLiuLu(double daDanLiuLu) {
        this.daDanLiuLu = daDanLiuLu;
    }

    public double getJinDaDan() {
        return jinDaDan;
    }

    public void setJinDaDan(double jinDaDan) {
        this.jinDaDan = jinDaDan;
    }

    public double getJinDaDanZhanBi() {
        return jinDaDanZhanBi;
    }

    public void setJinDaDanZhanBi(double jinDaDanZhanBi) {
        this.jinDaDanZhanBi = jinDaDanZhanBi;
    }

    public double getDaDanChengJiaEr() {
        return daDanChengJiaEr;
    }

    public void setDaDanChengJiaEr(double daDanChengJiaEr) {
        this.daDanChengJiaEr = daDanChengJiaEr;
    }

    public double getDaDanZhanBi() {
        return daDanZhanBi;
    }

    public void setDaDanZhanBi(double daDanZhanBi) {
        this.daDanZhanBi = daDanZhanBi;
    }

    public double getZhongDanLiuLu() {
        return zhongDanLiuLu;
    }

    public void setZhongDanLiuLu(double zhongDanLiuLu) {
        this.zhongDanLiuLu = zhongDanLiuLu;
    }

    public double getZhongDanLiuChu() {
        return zhongDanLiuChu;
    }

    public void setZhongDanLiuChu(double zhongDanLiuChu) {
        this.zhongDanLiuChu = zhongDanLiuChu;
    }

    public double getJinZhongDanZhangBi() {
        return jinZhongDanZhangBi;
    }

    public void setJinZhongDanZhangBi(double jinZhongDanZhangBi) {
        this.jinZhongDanZhangBi = jinZhongDanZhangBi;
    }

    public double getJinZhongDan() {
        return jinZhongDan;
    }

    public void setJinZhongDan(double jinZhongDan) {
        this.jinZhongDan = jinZhongDan;
    }

    public double getZhongDanChenJiaoEr() {
        return zhongDanChenJiaoEr;
    }

    public void setZhongDanChenJiaoEr(double zhongDanChenJiaoEr) {
        this.zhongDanChenJiaoEr = zhongDanChenJiaoEr;
    }

    public double getXiaoDanLiuLu() {
        return xiaoDanLiuLu;
    }

    public void setXiaoDanLiuLu(double xiaoDanLiuLu) {
        this.xiaoDanLiuLu = xiaoDanLiuLu;
    }

    public double getXiaoDanLiuChu() {
        return xiaoDanLiuChu;
    }

    public void setXiaoDanLiuChu(double xiaoDanLiuChu) {
        this.xiaoDanLiuChu = xiaoDanLiuChu;
    }

    public double getJinXiaoDanZhangBi() {
        return jinXiaoDanZhangBi;
    }

    public void setJinXiaoDanZhangBi(double jinXiaoDanZhangBi) {
        this.jinXiaoDanZhangBi = jinXiaoDanZhangBi;
    }

    public double getJinXiaoDan() {
        return jinXiaoDan;
    }

    public void setJinXiaoDan(double jinXiaoDan) {
        this.jinXiaoDan = jinXiaoDan;
    }

    public double getXiaoDanChenJiaoEr() {
        return xiaoDanChenJiaoEr;
    }

    public void setXiaoDanChenJiaoEr(double xiaoDanChenJiaoEr) {
        this.xiaoDanChenJiaoEr = xiaoDanChenJiaoEr;
    }
}
