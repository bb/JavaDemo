package com.bing.service;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Administrator on 2015/7/16.
 */
public interface BaseService<T, K extends Serializable> {
    void save(T entity);

    void update(T entity);

    void delete(K id);

    T getById(K id);

    List<T> getByHQL(String hql, Object... params);

    T findByHQLOne(String hql, Object... params);
}
