package com.bing.service.impl;

import com.bing.dao.BaseDao;
import com.bing.service.BaseService;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.io.Serializable;
import java.util.List;

/**
 * Created by Administrator on 2015/7/16.
 */

@Transactional
public class BaseServiceImpl<T, K extends Serializable> implements BaseService<T, K> {

    @Resource
    private BaseDao<T, K> dao;

    @Override
    public void save(T entity) {
        dao.save(entity);
    }

    @Override
    public void update(T entity) {
        dao.update(entity);
    }

    @Override
    public void delete(K id) {
        dao.delete(id);
    }

    @Override
    public T getById(K id) {
        return dao.findById(id);
    }

    @Override
    public List<T> getByHQL(String hql, Object... params) {
        return dao.findByHQL(hql, params);
    }

    @Override
    public T findByHQLOne(String hql, Object... params) {
        return null;
    }
}
