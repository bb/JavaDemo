package com.bing.service.impl;

import com.bing.dao.StockDao;
import com.bing.entity.Stock;
import com.bing.service.StockService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * Created by Administrator on 2015/7/16.
 */
@Service("stockService")
@Transactional
public class StockServiceImpl<T> extends BaseServiceImpl<Stock, Long> implements StockService {
    @Resource(name = "stockDao")
    private StockDao stockDao;
}
