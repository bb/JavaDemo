package com.bing.util;

import com.bing.entity.Stock;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;
import java.util.List;

/**
 * Created by Administrator on 2015/7/13.
 */
public class StockUtil {


    private static final String FETCH_STOCKS_URL = "http://file1.yztz.com/js/goods.js";

    private static final String USER_AGENT = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.132 Safari/537.36";

    public static List<Stock> fetchStocks() throws IOException {
        Document doc = Jsoup.connect(FETCH_STOCKS_URL).ignoreContentType(true).header("Accept-Language","zh-CN").header("Content-Type","application/javascript").userAgent(USER_AGENT).timeout(10000).get();
        String text = doc.text();
        text=new String(text.getBytes("iso8859_1"),"gbk");
        System.out.println(text);

        return null;
    }
}
