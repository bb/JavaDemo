package com.bing;

import com.bing.bean.Stock;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.Test;

import javax.script.ScriptException;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Administrator on 2015/5/27.
 */
public class HelloStock {

    Pattern pattern = Pattern.compile("共(?<count>\\d{1,})条");    //共50条
    Pattern fenlei = Pattern.compile("id:(?<id>\\d{1,}),code:'(?<code>\\d{1,})',name:'(?<name>.*?)',industry:'(?<industry>.*?,\\d{1,})'");    //id:44204,code:'600061',name:'中纺投资',industry:'券商,10633'
    Pattern fa = Pattern.compile("_id:(?<id>\\d{1,}),_name:'(?<name>.*?)'");   //_id:10623,_name:'计算机',_ud:3.83,_a:120724577297,_cio:10112448507,_cior:8.38,_vc:1.30,_t:6.54,_uc:91,_tc:23,_dc:22,id:51355,code:'603918',name:'金桥信息'

    private static final String USER_AGENT = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.132 Safari/537.36";

    @Test
    public void test() throws IOException, ScriptException {
        Document doc = Jsoup.connect("http://hq.sinajs.cn/list=sh601003,sh601001,sh601002,sh601005,sh601008,sh601009").ignoreContentType(true).get();
        String text = doc.text();
        System.out.println(text);
        Matcher matcher = Pattern.compile("hq_str_(?<daihao>.*?)=\"(?<detail>[^\"]*?)\";").matcher(text);
        while (matcher.find()) {
            String daihao = matcher.group("daihao");
            String detail = matcher.group("detail");
            System.out.println(Stock.fromString(daihao, detail).toString());
        }
    }

    /**
     * http://quote.yztz.com/stock_code.jsp
     */
    @Test
    public void testGetAll() throws IOException {
        Document doc = Jsoup.connect("http://quote.yztz.com/stock_code.jsp").userAgent(USER_AGENT).timeout(4000).get();
        Elements select = doc.select("#news table tr td a.f14");
//        Elements select = doc.select("#news table tr:gt(2) td.trh");
        for (int i = 0; i < select.size(); i++) {
            System.out.println(String.format("%10s:%s", select.get(i).text(), select.get(++i).text()));

        }
    }


    /**
     * http://quote.yztz.com/quote/sector_10623
     */
    @Test
    public void testGetHangYe() throws IOException {
        Document doc = Jsoup.connect("http://quote.yztz.com/quote/sector.jsp?sid=10623").get();

        Elements elements = doc.select("#list-body ul[g~=id:\\d{1,},code]");
        Matcher matcher = pattern.matcher(doc.select("#pages i").text());
        int count = 0;
        if (matcher.find()) {
            count = Integer.valueOf(matcher.group("count"));
        }

        for (Element element : elements) {
            String g = element.attr("g");
            matcher = fenlei.matcher(g);
            if (matcher.find()) {
                System.out.println(matcher.group("code") + "   " + matcher.group("name") + "  " + matcher.group("industry"));
            }

        }
        for (int i = 2; i < count / elements.size() + 2; i++) {
            doc = Jsoup.connect("http://quote.yztz.com/quote/sector.jsp?sid=10623&p=" + i).get();
            elements = doc.select("#list-body ul[g~=id:\\d{1,},code]");
            for (Element element : elements) {
                String g = element.attr("g");
                matcher = fenlei.matcher(g);
                if (matcher.find()) {
                    System.out.println(matcher.group("code") + "   " + matcher.group("name") + "  " + matcher.group("industry"));
                }

            }
        }

    }


    @Test

    public void testMbc() throws IOException {
        Document doc = Jsoup.connect("http://quote.yztz.com/quote/sector_industry.jsp").get();
        Elements elements = doc.select("#list-body ul[s^=_id:]");
        for (Element element : elements) {
            String s = element.attr("s");
            Matcher matcher = fa.matcher(s);
            if (matcher.find()) {
                System.out.println(matcher.group("id") + "  " + matcher.group("name"));
            }
        }

    }
}
