package com.bing;

import org.hibernate.tool.hbm2ddl.SchemaExport;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;

/**
 * Created by Administrator on 2015/7/12.
 */
public class SchemaExportTest {
    LocalSessionFactoryBean sessionFactoryBean;
    @Before
    public void before() {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("application.xml");
        sessionFactoryBean = (LocalSessionFactoryBean) applicationContext.getBean("&sessionFactory");
    }

    @Test
    public void test() {
        SchemaExport schemaExport = new SchemaExport(sessionFactoryBean.getConfiguration());
        schemaExport.create(true, true);

    }
}
