package com.bing.dao.impl;

import com.bing.entity.Stock;
import com.bing.service.StockService;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.List;

/**
 * Created by Administrator on 2015/7/15.
 */
public class StockDaoImplTest {


    private StockService stockService;

    @Before
    public void setUp() {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("application.xml");
        stockService = (StockService) context.getBean("stockService");
    }

    @Test
    public void testHelloSpringDao() {
        Stock stock = new Stock();
        stock.setName("招商银行2");
        stock.setCode("555555");
        stock.getPinYin().add("ZSYsX");
        stockService.save(stock);
        List<Stock> byHQL = stockService.getByHQL("from Stock");
        for (Stock stock1 : byHQL) {
            System.out.println(stock1);
        }
    }


}