package com.bing.bean;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Administrator on 2015/7/9.
 */

@Entity
@Table(name = "AnnotationEvent" )
public class AnnotationEvent {
    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "titles")
    private String title;

    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date date;

    public AnnotationEvent() {
    }

    public AnnotationEvent(String title, Date date) {
        this.title = title;
        this.date = date;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
