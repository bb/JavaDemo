package com.bing.bean;

import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Administrator on 2015/7/11.
 */

@Entity
@Audited
public class AuditeEvent {
    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "titles")
    private String title;

    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date date;

    public AuditeEvent() {
    }

    public AuditeEvent(String title, Date date) {
        this.title = title;
        this.date = date;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
