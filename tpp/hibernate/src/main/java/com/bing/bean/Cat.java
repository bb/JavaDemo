package com.bing.bean;

import org.hibernate.annotations.Type;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Set;

/**
 * Created by Administrator on 2015/7/11.
 */
@Entity
@Table
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "subClass")
@DiscriminatorValue("cat")
public class Cat {
    @Id
    @GeneratedValue

    private Long id;
    @Column(nullable = false, updatable = false)
    private int age;

    @Column(nullable = false, updatable = false)
    private String sex;

    @Column()
    private String name;

    @Type(type = "com.bing.bean.Color")

    private Color color;

    @OneToMany(targetEntity = Cat.class, mappedBy = "mother",orphanRemoval = true)
    private Set<Cat> kittens;

    @ManyToOne(targetEntity = Cat.class)
    @JoinColumn(name = "ref_mother" ,nullable = false)
    private Cat mother;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getName() {
        return name;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Cat> getKittens() {
        return kittens;
    }

    public void setKittens(Set<Cat> kittens) {
        this.kittens = kittens;
    }

    public Cat getMother() {
        return mother;
    }

    public void setMother(Cat mother) {
        this.mother = mother;
    }
}
