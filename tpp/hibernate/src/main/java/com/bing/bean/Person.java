package com.bing.bean;

import java.util.Set;

/**
 * Created by Administrator on 2015/7/11.
 */
public class Person {
    private Long id;
    private String age;
    private String firstName;
    private String lastName;
    private Set<Event> events;
    private Set emailAddrs;

    public Set<Event> getEvents() {
        return events;
    }

    public void setEvents(Set<Event> events) {
        this.events = events;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Set getEmailAddrs() {
        return emailAddrs;
    }

    public void setEmailAddrs(Set emailAddrs) {
        this.emailAddrs = emailAddrs;
    }
}
