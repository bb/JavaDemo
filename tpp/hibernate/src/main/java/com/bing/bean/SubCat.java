package com.bing.bean;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * Created by Administrator on 2015/7/11.
 */

@Entity
@DiscriminatorValue("D")
public class SubCat extends Cat {

    @Column
    private String reqi;

    public String getReqi() {
        return reqi;
    }

    public void setReqi(String reqi) {
        this.reqi = reqi;
    }

}
