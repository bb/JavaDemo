package com.bing.bean;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Transient;

/**
 * Created by Administrator on 2015/7/11.
 */
@Entity
public class User {

    @EmbeddedId
    @AttributeOverride(name = "firstName", column = @Column(name = "fname"))
    private UserId userId;

    @Column
    private int age;

    private String tras;

    @Transient
    private String transients;

    public UserId getUserId() {
        return userId;
    }

    public void setUserId(UserId userId) {
        this.userId = userId;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
