package com.bing.bean;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Date;
import java.util.List;

/**
 * Created by Administrator on 2015/7/9.
 */


public class AnnotationEventTest {
    private SessionFactory sessionFactory;

    @Before
    public void before() {
        sessionFactory = new Configuration().configure().buildSessionFactory();
    }

    @After
    public void tearDown() throws Exception {
        if (sessionFactory != null) {
            sessionFactory.close();
        }
    }

    @Test
    public void helloAnnotateEvent(){
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        session.save(new AnnotationEvent("title", new Date()));
        session.save(new AnnotationEvent("中文title", new Date()));

        session.getTransaction().commit();
        session.close();

        session = sessionFactory.openSession();
        session.beginTransaction();

        List result = session.createQuery( "from AnnotationEvent" ).list();
        for ( AnnotationEvent event : (List<AnnotationEvent>) result ) {
            System.out.println( "Event (" + event.getDate() + ") : " + event.getTitle() );
        }
        session.getTransaction().commit();
        session.close();
    }


}