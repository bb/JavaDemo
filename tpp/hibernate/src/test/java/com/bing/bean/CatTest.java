package com.bing.bean;

import com.bing.bean.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by Administrator on 2015/7/11.
 */
public class CatTest {

    SessionFactory sessionFactory;

    @Before
    public void setUp() throws Exception {

        sessionFactory = HibernateUtil.getSessioniFactory();
    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void testInsertCat() throws InterruptedException {
        Session session = sessionFactory.getCurrentSession();
        Cat cat = new Cat();
        cat.setName("bing");
        cat.setColor(Color.BLACK);
        cat.setAge(14);
        cat.setSex("f");

        SubCat subCat = new SubCat();
        subCat.setName("subCat");
        subCat.setColor(Color.RED);
        subCat.setAge(44);
        subCat.setSex("f");
        subCat.setMother(cat);

        session.beginTransaction();
        session.save(cat);
        session.save(subCat);
        session.getTransaction().commit();
        session = sessionFactory.getCurrentSession();
        session.beginTransaction();
        cat.setSex("m");
        session.getTransaction().commit();

        Thread.sleep(1000);
    }

}