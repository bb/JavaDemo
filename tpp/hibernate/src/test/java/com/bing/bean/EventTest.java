package com.bing.bean;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.junit.Test;

import java.util.Date;
import java.util.List;

/**
 * Created by Administrator on 2015/7/9.
 */
public class EventTest {


    @Test
    public void testHibernate() {
        Configuration configuration = new Configuration();
        SessionFactory sessionFactory = configuration.configure().buildSessionFactory();
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        session.save(new Event("title2", new Date()));
        session.save(new Event("A follow up event2", new Date()));
        session.getTransaction().commit();

        session.close();
        session= sessionFactory.openSession();
        session.beginTransaction();
        List result = session.createQuery("from Event").list();
        for (Event event : (List<Event>) result) {
            System.out.println("Event (" + event.getDate() + ") : " + event.getTitle());
        }
        session.getTransaction().commit();
        session.close();
    }
}
