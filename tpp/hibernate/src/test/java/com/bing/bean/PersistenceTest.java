package com.bing.bean;

import org.hibernate.envers.AuditReader;
import org.hibernate.envers.AuditReaderFactory;
import org.junit.Before;
import org.junit.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by Administrator on 2015/7/11.
 */
public class PersistenceTest {
    private EntityManagerFactory entityManagerFactory;

    @Before
    public void before(){
        entityManagerFactory = Persistence.createEntityManagerFactory("auditEvent");
    }

    @Test
    public void helloPersistence(){
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        entityManager.persist(new AnnotationEvent("testPersistence", new Date()));
        entityManager.persist(new AnnotationEvent("中文testPersistence", new Date()));
        entityManager.getTransaction().commit();

        entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        List<AnnotationEvent> result = entityManager.createQuery( "from AnnotationEvent ", AnnotationEvent.class ).getResultList();
        for ( AnnotationEvent event : result ) {
            System.out.println( "Event (" + event.getDate() + ") : " + event.getTitle() );
        }
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    public void testBasicUsage() {
        // create a couple of events
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        entityManager.persist( new AuditeEvent( "Our very first event!", new Date() ) );
        entityManager.persist( new AuditeEvent( "A follow up event", new Date() ) );
        entityManager.getTransaction().commit();
        entityManager.close();

        // now lets pull events from the database and list them
        entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        List<AuditeEvent> result = entityManager.createQuery( "from AuditeEvent", AuditeEvent.class ).getResultList();
        for ( AuditeEvent event : result ) {
            System.out.println( "Event (" + event.getDate() + ") : " + event.getTitle() );
        }
        entityManager.getTransaction().commit();
        entityManager.close();

        // so far the code is the same as we have seen in previous tutorials.  Now lets leverage Envers...

        // first lets create some revisions
        entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        AuditeEvent myEvent = entityManager.find( AuditeEvent.class, 2L ); // we are using the increment generator, so we know 2 is a valid id
        myEvent.setDate( new Date() );
        myEvent.setTitle( myEvent.getTitle() + " (rescheduled)" );
        entityManager.getTransaction().commit();
        entityManager.close();

        // and then use an AuditReader to look back through history
        entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        myEvent = entityManager.find( AuditeEvent.class, 2L );
        assertEquals( "A follow up event (rescheduled)", myEvent.getTitle() );
        AuditReader reader = AuditReaderFactory.get(entityManager);
        AuditeEvent firstRevision = reader.find( AuditeEvent.class, 2L, 1 );
        assertFalse( firstRevision.getTitle().equals( myEvent.getTitle() ) );
        assertFalse( firstRevision.getDate().equals( myEvent.getDate() ) );
        AuditeEvent secondRevision = reader.find( AuditeEvent.class, 2L, 2 );
        assertTrue( secondRevision.getTitle().equals( myEvent.getTitle() ) );
        assertTrue( secondRevision.getDate().equals( myEvent.getDate() ) );
        entityManager.getTransaction().commit();
        entityManager.close();
    }
}
