package com.bing.bean;

import com.bing.bean.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by Administrator on 2015/7/11.
 */
public class PersonTest {
    SessionFactory sessionFactory;

    @Before
    public void setUp() throws Exception {

        sessionFactory = HibernateUtil.getSessioniFactory();
    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void testHelloPerson() {
        Session currentSession = sessionFactory.getCurrentSession();
        currentSession.beginTransaction();
        Person person = (Person) currentSession.load(Person.class, 1l);
        Event event = (Event) currentSession.load(Event.class, 1l);
        person.getEvents().add(event);     /*auto dirty check,处于持久态的实体,会在清理缓存时自动更新,当应用程序 调用org.hibernate.Transaction的commit()方法的时候.commit方法先清理缓存，然后再向数据库提交事务。 Hibernate之所以把清理缓存的时间点安排在事务快结束时，一方面是因为可以减少访问数据库的频率，还有一方面是因为可以尽可能缩短当前事务对数据 库中相关资源的锁定时间。
        当应用程序执行一些查询操作时，如果缓存中持久化对象的属性已经发生了变化，就会清理缓存，使得Session缓存与数据库已经进行了同步，从而保证查询结果返回的是正确的数据。
        当应用程序显示调用Session的flush()方法的时候。*/
        currentSession.getTransaction().commit();
    }

    @Test
    public void testPersonEmail() {
        Session currentSession = sessionFactory.getCurrentSession();
        currentSession.beginTransaction();
        Person aPerson = (Person) currentSession
                .createQuery("select p from Person p left join fetch p.events where p.id = :pid")
                .setParameter("pid", 1l)
                .uniqueResult(); // Eager fetch the collection so we can use it detached        System.out.println(person.getEmailAddrs());
        aPerson.getEmailAddrs().add("kaibing.@mail.com");
        currentSession.getTransaction().commit();
    }
}