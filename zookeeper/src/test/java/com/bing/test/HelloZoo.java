package com.bing.test;

import org.apache.zookeeper.*;
import org.junit.Test;

import java.io.IOException;

/**
 * Created by Administrator on 2015/5/17.
 */
public class HelloZoo {
    @Test
    public void testHelloZ() throws IOException, KeeperException, InterruptedException {
        ZooKeeper zk = new ZooKeeper("127.0.0.1:32181", 50000, new Watcher() {
            @Override
            public void process(WatchedEvent watchedEvent) {
                System.out.println(watchedEvent.toString());
            }
        });

        //创建一个节点root，数据是mydata,不进行ACL权限控制，节点为永久性的(即客户端shutdown了也不会消失)
        zk.create("/roots", "mydata".getBytes(), ZooDefs.Ids.OPEN_ACL_UNSAFE, CreateMode.PERSISTENT);

//在root下面创建一个childone znode,数据为childone,不进行ACL权限控制，节点为永久性的
        zk.create("/roots/childone", "childone".getBytes(), ZooDefs.Ids.OPEN_ACL_UNSAFE, CreateMode.PERSISTENT);

//取得/root节点下的子节点名称,返回List<String>
        for (String s : zk.getChildren("/roots", true)) {
            System.out.println("children:" + s);
        }

//取得/roots/childone节点下的数据,返回byte[]
        zk.getData("/roots/childone", true, null);

//修改节点/roots/childone下的数据，第三个参数为版本，如果是-1，那会无视被修改的数据版本，直接改掉
        zk.setData("/roots/childone", "childonemodify".getBytes(), -1);

//删除/roots/childone这个节点，第二个参数为版本，－1的话直接删除，无视版本
        zk.delete("/roots/childone", -1);

//关闭session
        zk.close();
    }

}
